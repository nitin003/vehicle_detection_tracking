import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import cv2
import glob
import time
from sklearn.svm import LinearSVC
from sklearn.preprocessing import StandardScaler
from skimage.feature import hog
from sklearn.model_selection import train_test_split
from Lane_finder import *
from scipy.ndimage.measurements import label

class Vehicle_detect():

    cars = []
    notcars = []
    color_space = 'LUV'  # Can be RGB, HSV, LUV, HLS, YUV, YCrCb
    orient = 9  # HOG orientations
    pix_per_cell = 8  # HOG pixels per cell
    cell_per_block = 2  # HOG cells per block
    hog_channel = "ALL"  # Can be 0, 1, 2, or "ALL"
    spatial_size = (16, 16)  # Spatial binning dimensions
    hist_bins = 32  # Number of histogram bins
    spatial_feat = True  # Spatial features on or off
    hist_feat = True  # Histogram features on or off
    hog_feat = True  # HOG features on or off
    y_start_stop = [400, 660]  # Min and max in y to search in slide_window()
    threshold = 2
    X_scaler = 0
    svc = 0

    def __init__(self):
        pass

    def get_hog_features(self, img, orient, pix_per_cell, cell_per_block,
                         vis=False, feature_vec=True):
        # Call with two outputs if vis==True
        if vis == True:
            features, hog_image = hog(img, orientations=self.orient,
                                      pixels_per_cell=(self.pix_per_cell, self.pix_per_cell),
                                      cells_per_block=(self.cell_per_block, self.cell_per_block),
                                      transform_sqrt=False,
                                      visualise=vis, feature_vector=feature_vec)
            return features, hog_image
        # Otherwise call with one output
        else:
            features = hog(img, orientations=self.orient,
                           pixels_per_cell=(self.pix_per_cell, self.pix_per_cell),
                           cells_per_block=(self.cell_per_block, self.cell_per_block),
                           transform_sqrt=False,
                           visualise=False, feature_vector=feature_vec)
            return features

    # Define a function to compute binned color features
    def bin_spatial(self, img, size=(32, 32)):
        # Use cv2.resize().ravel() to create the feature vector
        features = cv2.resize(img, size).ravel()
        # Return the feature vector
        return features

        # Define a function to compute color histogram features

    def color_hist(self, img, nbins=32, bins_range=(0, 256)):
        # Compute the histogram of the color channels separately
        channel1_hist = np.histogram(img[:, :, 0], bins=nbins, range=bins_range)
        channel2_hist = np.histogram(img[:, :, 1], bins=nbins, range=bins_range)
        channel3_hist = np.histogram(img[:, :, 2], bins=nbins, range=bins_range)
        hist_features = np.concatenate((channel1_hist[0], channel2_hist[0], channel3_hist[0]))
        return hist_features

    # Have this function call bin_spatial() and color_hist()
    def extract_features(self, imgs, color_space='RGB', spatial_size=(32, 32),
                         hist_bins=32, orient=9,
                         pix_per_cell=8, cell_per_block=2, hog_channel=0,
                         spatial_feat=True, hist_feat=True, hog_feat=True):
        # Create a list to append feature vectors to
        features = []
        # Iterate through the list of images
        for file in imgs:
            file_features = []
            # Read in each one by one
            image = mpimg.imread(file)
            # apply color conversion if other than 'RGB'
            if color_space != 'RGB':
                if color_space == 'HSV':
                    feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
                elif color_space == 'LUV':
                    feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2LUV)
                elif color_space == 'HLS':
                    feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2HLS)
                elif color_space == 'YUV':
                    feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2YUV)
                elif color_space == 'YCrCb':
                    feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2YCrCb)
            else:
                feature_image = np.copy(image)

            if spatial_feat == True:
                spatial_features = self.bin_spatial(feature_image, size=spatial_size)
                file_features.append(spatial_features)
            if hist_feat == True:
                # Apply color_hist()
                hist_features = self.color_hist(feature_image, nbins=hist_bins)
                file_features.append(hist_features)
            if hog_feat == True:
                # Call get_hog_features() with vis=False, feature_vec=True
                if hog_channel == 'ALL':
                    hog_features = []
                    for channel in range(feature_image.shape[2]):
                        hog_features.append(self.get_hog_features(feature_image[:, :, channel],
                                                             orient, pix_per_cell, cell_per_block,
                                                             vis=False, feature_vec=True))
                    hog_features = np.ravel(hog_features)
                else:
                    hog_features = self.get_hog_features(feature_image[:, :, hog_channel], orient,
                                                    pix_per_cell, cell_per_block, vis=False, feature_vec=True)
                # Append the new feature vector to the features list
                file_features.append(hog_features)
            features.append(np.concatenate(file_features))
        # Return list of feature vectors
        return features

    def slide_window(self, img, x_start_stop=[None, None], y_start_stop=[None, None],
                     xy_window=(64, 64), xy_overlap=(0.5, 0.5)):
        # If x and/or y start/stop positions not defined, set to image size
        if x_start_stop[0] == None:
            x_start_stop[0] = 0
        if x_start_stop[1] == None:
            x_start_stop[1] = img.shape[1]
        if y_start_stop[0] == None:
            y_start_stop[0] = 0
        if y_start_stop[1] == None:
            y_start_stop[1] = img.shape[0]
        # Compute the span of the region to be searched
        xspan = x_start_stop[1] - x_start_stop[0]
        yspan = y_start_stop[1] - y_start_stop[0]
        # Compute the number of pixels per step in x/y
        nx_pix_per_step = np.int(xy_window[0] * (1 - xy_overlap[0]))
        ny_pix_per_step = np.int(xy_window[1] * (1 - xy_overlap[1]))
        # Compute the number of windows in x/y
        nx_buffer = np.int(xy_window[0] * (xy_overlap[0]))
        ny_buffer = np.int(xy_window[1] * (xy_overlap[1]))
        nx_windows = np.int((xspan - nx_buffer) / nx_pix_per_step)
        ny_windows = np.int((yspan - ny_buffer) / ny_pix_per_step)
        # Initialize a list to append window positions to
        window_list = []
        # Loop through finding x and y window positions
        #     Note: you could vectorize this step, but in practice
        #     you'll be considering windows one by one with your
        #     classifier, so looping makes sense
        # Calculate each window position
        # Append window position to list
        # Return the list of windows
        for ys in range(ny_windows):
            for xs in range(nx_windows):
                # Calculate window position
                startx = xs * nx_pix_per_step + x_start_stop[0]
                endx = startx + xy_window[0]
                starty = ys * ny_pix_per_step + y_start_stop[0]
                endy = starty + xy_window[1]
                # Append window position to list
                window_list.append(((startx, starty), (endx, endy)))
        return window_list

    def draw_boxes(self, img, bboxes, color=(255, 0, 255), thick=6):
        # Make a copy of the image
        imcopy = np.copy(img)
        # Iterate through the bounding boxes
        for bbox in bboxes:
            # Draw a rectangle given bbox coordinates
            cv2.rectangle(imcopy, bbox[0], bbox[1], color, thick)
        # Return the image copy with boxes drawn
        return imcopy

    def single_img_features(self, img, color_space='RGB', spatial_size=(32, 32),
                            hist_bins=32, orient=9,
                            pix_per_cell=8, cell_per_block=2, hog_channel=0,
                            spatial_feat=True, hist_feat=True, hog_feat=True):
        # 1) Define an empty list to receive features
        img_features = []
        # 2) Apply color conversion if other than 'RGB'
        if color_space != 'RGB':
            if color_space == 'HSV':
                feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
            elif color_space == 'LUV':
                feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2LUV)
            elif color_space == 'HLS':
                feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
            elif color_space == 'YUV':
                feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
            elif color_space == 'YCrCb':
                feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2YCrCb)
        else:
            feature_image = np.copy(img)
        # 3) Compute spatial features if flag is set
        if spatial_feat == True:
            spatial_features = self.bin_spatial(feature_image, size=spatial_size)
            # 4) Append features to list
            img_features.append(spatial_features)
        # 5) Compute histogram features if flag is set
        if hist_feat == True:
            hist_features = self.color_hist(feature_image, nbins=hist_bins)
            # 6) Append features to list
            img_features.append(hist_features)
        # 7) Compute HOG features if flag is set
        if hog_feat == True:
            if hog_channel == 'ALL':
                hog_features = []
                for channel in range(feature_image.shape[2]):
                    hog_features.extend(self.get_hog_features(feature_image[:, :, channel],
                                                         orient, pix_per_cell, cell_per_block,
                                                         vis=False, feature_vec=True))
            else:
                hog_features = self.get_hog_features(feature_image[:, :, hog_channel], orient,
                                                pix_per_cell, cell_per_block, vis=False, feature_vec=True)
            # 8) Append features to list
            img_features.append(hog_features)

        # 9) Return concatenated array of features
        return np.concatenate(img_features)

    def search_windows(self, img, windows, clf, scaler, color_space='RGB',
                       spatial_size=(32, 32), hist_bins=32,
                       hist_range=(0, 256), orient=8,
                       pix_per_cell=8, cell_per_block=2,
                       hog_channel=0, spatial_feat=True,
                       hist_feat=True, hog_feat=True):

        # 1) Create an empty list to receive positive detection windows
        on_windows = []
        # 2) Iterate over all windows in the list
        for window in windows:
            # 3) Extract the test window from original image
            test_img = cv2.resize(img[window[0][1]:window[1][1], window[0][0]:window[1][0]], (64, 64))
            # 4) Extract features for that window using single_img_features()
            features = self.single_img_features(test_img, color_space=color_space,
                                           spatial_size=spatial_size, hist_bins=hist_bins,
                                           orient=orient, pix_per_cell=pix_per_cell,
                                           cell_per_block=cell_per_block,
                                           hog_channel=hog_channel, spatial_feat=spatial_feat,
                                           hist_feat=hist_feat, hog_feat=hog_feat)
            # 5) Scale extracted features to be fed to classifier
            test_features = scaler.transform(np.array(features).reshape(1, -1))
            # 6) Predict using your classifier
            prediction = clf.predict(test_features)
            # 7) If positive (prediction == 1) then save the window
            if prediction == 1:
                on_windows.append(window)
        # 8) Return windows for positive detections
        return on_windows

    def convert_color(self, img, conv='RGB2YCrCb'):
        if conv == 'RGB2YCrCb':
            return cv2.cvtColor(img, cv2.COLOR_RGB2YCrCb)
        if conv == 'BGR2YCrCb':
            return cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
        if conv == 'RGB2LUV':
            return cv2.cvtColor(img, cv2.COLOR_RGB2LUV)

    def find_cars(self, img, ystart, ystop, scale, svc, X_scaler, orient, pix_per_cell, cell_per_block, spatial_size,
                  hist_bins):

        boxes = []
        draw_img = np.copy(img)
        img = img.astype(np.float32) / 255

        # plt.imshow(img)
        # plt.show()

        img_tosearch = img[ystart:ystop, :, :]
        ctrans_tosearch = self.convert_color(img_tosearch, conv='RGB2LUV')
        # print(ctrans_tosearch.shape)
        # plt.imshow(ctrans_tosearch)
        # plt.show()
        if scale != 1:
            imshape = ctrans_tosearch.shape
            ctrans_tosearch = cv2.resize(ctrans_tosearch, (np.int(imshape[1] / scale), np.int(imshape[0] / scale)))

        # select colorspace channel for HOG
        if self.hog_channel == 'ALL':
            ch1 = ctrans_tosearch[:, :, 0]
            ch2 = ctrans_tosearch[:, :, 1]
            ch3 = ctrans_tosearch[:, :, 2]
        else:
             ch1 = ctrans_tosearch[:, :, self.hog_channel]

        # Define blocks and steps as above
        nxblocks = (ch1.shape[1] // pix_per_cell) - 1
        nyblocks = (ch1.shape[0] // pix_per_cell) - 1
        nfeat_per_block = orient * cell_per_block ** 2

        # 64 was the orginal sampling rate, with 8 cells and 8 pix per cell
        window = 64
        nblocks_per_window = (window // pix_per_cell) - 1
        cells_per_step = 2  # Instead of overlap, define how many cells to step
        nxsteps = (nxblocks - nblocks_per_window) // cells_per_step
        nysteps = (nyblocks - nblocks_per_window) // cells_per_step

        # Compute individual channel HOG features for the entire image
        hog1 = self.get_hog_features(ch1, orient, pix_per_cell, cell_per_block, feature_vec=False)
        if self.hog_channel == 'ALL':
            hog2 = self.get_hog_features(ch2, orient, pix_per_cell, cell_per_block, feature_vec=False)
            hog3 = self.get_hog_features(ch3, orient, pix_per_cell, cell_per_block, feature_vec=False)

        for xb in range(nxsteps):
            for yb in range(nysteps):
                ypos = yb * cells_per_step
                xpos = xb * cells_per_step
                # Extract HOG for this patch
                hog_feat1 = hog1[ypos:ypos + nblocks_per_window, xpos:xpos + nblocks_per_window].ravel()
                if self.hog_channel == 'ALL':
                    hog_feat2 = hog2[ypos:ypos + nblocks_per_window, xpos:xpos + nblocks_per_window].ravel()
                    hog_feat3 = hog3[ypos:ypos + nblocks_per_window, xpos:xpos + nblocks_per_window].ravel()
                    hog_features = np.hstack((hog_feat1, hog_feat2, hog_feat3))
                else:
                    hog_features = hog_feat1

                xleft = xpos * pix_per_cell
                ytop = ypos * pix_per_cell

                # Extract the image patch
                #subimg = cv2.resize(ctrans_tosearch[ytop:ytop + window, xleft:xleft + window], (64, 64))
                subimg = ctrans_tosearch[ytop:ytop+window, xleft:xleft+window]

                # Get color features
                spatial_features = self.bin_spatial(subimg, size=spatial_size)
                hist_features = self.color_hist(subimg, nbins=hist_bins)

                # Scale features and make a prediction
                test_features = X_scaler.transform(np.hstack((spatial_features, hist_features, hog_features)).reshape(1, -1))
                # test_features = X_scaler.transform(np.hstack((shape_feat, hist_feat)).reshape(1, -1))
                test_prediction = svc.predict(test_features)

                if test_prediction == 1:
                    xbox_left = np.int(xleft * scale)
                    ytop_draw = np.int(ytop * scale)
                    win_draw = np.int(window * scale)
                    boxes.append(((int(xbox_left), int(ytop_draw + ystart)),
                                  (int(xbox_left + win_draw), int(ytop_draw + win_draw + ystart))))
        return boxes

    def add_heat(self, heatmap, bbox_list):
        # Iterate through list of bboxes
        for box in bbox_list:
            # Add += 1 for all pixels inside each bbox
            # Assuming each "box" takes the form ((x1, y1), (x2, y2))
            heatmap[box[0][1]:box[1][1], box[0][0]:box[1][0]] += 1
        return heatmap  # Return updated heatmap

    def apply_threshold(self, heatmap):  # Zero out pixels below the threshold in the heatmap
        heatmap[heatmap < self.threshold] = 0
        return heatmap

    def draw_labeled_bboxes(self, img, labels):
        boxes = []
        final_img = []
        # Iterate through all detected cars
        for car_number in range(1, labels[1] + 1):
            # Find pixels with each car_number label value
            nonzero = (labels[0] == car_number).nonzero()
            # Identify x and y values of those pixels
            nonzeroy = np.array(nonzero[0])
            nonzerox = np.array(nonzero[1])
            # Define a bounding box based on min/max x and y
            bbox = ((np.min(nonzerox), np.min(nonzeroy)), (np.max(nonzerox), np.max(nonzeroy)))
            boxes.append(bbox)
            # Draw the box on the image
            cv2.rectangle(img, bbox[0], bbox[1], (0, 0, 255), 6)

        # Return the image
        return img, boxes


    def print_len(self):
        print(len(self.cars), len(self.notcars))
        fig, axs = plt.subplots(8, 8, figsize=(16, 16))
        fig.subplots_adjust(hspace=.2, wspace=.001)
        axs = axs.ravel()

        # Step through the list and search for chessboard corners
        for i in np.arange(32):
            img = cv2.imread(self.cars[np.random.randint(0, len(self.cars))])
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            plt.imshow(img)
            plt.show()
        for i in np.arange(32, 64):
            img = cv2.imread(self.notcars[np.random.randint(0, len(self.notcars))])
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            axs[i].axis('off')
            axs[i].set_title('nope', fontsize=10)
            plt.imshow(img)
            plt.show()

    def process_vid(self, img):
        lane = pipeline(img)
        y_start_stop = [400, 500, 400, 540, 400, 580, 400, 620, 400, 660]
        scale = 1.0
        boxes = []
        for i in range(5):
            boxes += self.find_cars(img, y_start_stop[i * 2], y_start_stop[(i * 2) + 1], (scale + (i * 0.45)), self.svc,
                                    self.X_scaler, self.orient, self.pix_per_cell,
                                    self.cell_per_block, self.spatial_size, self.hist_bins)
            # print("boxes")
            # print(boxes)

        heatmap_img = np.zeros_like(img[:, :, 0])
        heatmap_img = self.add_heat(heatmap_img, boxes)
        heatmap_img = self.apply_threshold(heatmap_img)
        labels = label(heatmap_img)
        # print(labels[1], 'cars found')
        draw_img, new_box = self.draw_labeled_bboxes(np.copy(img), labels)
        final_img = self.draw_boxes(lane, new_box, color=(255, 0, 255), thick=6)
        return final_img


def main():
    detect = Vehicle_detect()
    # Read in cars and notcars
    detect.cars = glob.glob('vehicles/**/*.png')
    detect.notcars = glob.glob('non-vehicles/**/*.png')
    car_features = detect.extract_features(detect.cars, color_space=detect.color_space,
                                    spatial_size=detect.spatial_size, hist_bins=detect.hist_bins,
                                    orient=detect.orient, pix_per_cell=detect.pix_per_cell,
                                    cell_per_block=detect.cell_per_block,
                                    hog_channel=detect.hog_channel, spatial_feat=detect.spatial_feat,
                                    hist_feat=detect.hist_feat, hog_feat=detect.hog_feat)
    notcar_features = detect.extract_features(detect.notcars, color_space=detect.color_space,
                                       spatial_size=detect.spatial_size, hist_bins=detect.hist_bins,
                                       orient=detect.orient, pix_per_cell=detect.pix_per_cell,
                                       cell_per_block=detect.cell_per_block,
                                       hog_channel=detect.hog_channel, spatial_feat=detect.spatial_feat,
                                       hist_feat=detect.hist_feat, hog_feat=detect.hog_feat)

    X = np.vstack((car_features, notcar_features)).astype(np.float64)
    # Fit a per-column scaler
    detect.X_scaler = StandardScaler().fit(X)
    local_scaler = detect.X_scaler
    # Apply the scaler to X
    scaled_X = local_scaler.transform(X)

    # Define the labels vector
    y = np.hstack((np.ones(len(car_features)), np.zeros(len(notcar_features))))

    # Split up data into randomized training and test sets
    rand_state = np.random.randint(0, 100)
    X_train, X_test, y_train, y_test = train_test_split(
        scaled_X, y, test_size=0.2, random_state=rand_state)

    #print('Using:', detect.orient, 'orientations', detect.pix_per_cell,
    #      'pixels per cell and', detect.cell_per_block, 'cells per block')
    print('Feature vector length:', len(X_train[0]))
    # Use a linear SVC
    detect.svc = LinearSVC()
    # Check the training time for the SVC
    t = time.time()
    detect.svc.fit(X_train, y_train)
    t2 = time.time()
    print(round(t2 - t, 2), 'Seconds to train SVC...')
    # Check the score of the SVC
    print('Test Accuracy of SVC = ', round(detect.svc.score(X_test, y_test), 4))

    # for image in glob.glob('test_images/test*.jpg'):
    #     img1 = mpimg.imread(image)
    #     plt.imshow(img1)
    #     plt.show()
    #     #lane = pipeline(img)
    #
    #     # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    #     img = img1.astype(np.float32) / 255
    #     draw_image = np.copy(img)
    #     hot_windows = []
    #     windows = detect.slide_window(img, x_start_stop=[None, None], y_start_stop=[400,660], #detect.y_start_stop,
    #                            xy_window=(96, 96), xy_overlap=(0.78, 0.78))
    #
    #     hot_windows += detect.search_windows(img, windows, detect.svc, detect.X_scaler, color_space=detect.color_space,
    #                                         spatial_size=detect.spatial_size, hist_bins=detect.hist_bins,
    #                                         orient=detect.orient, pix_per_cell=detect.pix_per_cell,
    #                                         cell_per_block=detect.cell_per_block,
    #                                         hog_channel=detect.hog_channel, spatial_feat=detect.spatial_feat,
    #                                         hist_feat=detect.hist_feat, hog_feat=detect.hog_feat)
    #     temp = detect.draw_boxes(img1, windows, color=(255, 0, 255), thick=6)
    #     plt.imshow(temp)
    #     plt.show()
    #     heatmap_img = np.zeros_like(img[:, :, 0])
    #     heatmap_img = detect.add_heat(heatmap_img, hot_windows)
    #     heatmap_img = detect.apply_threshold(heatmap_img)
    #     plt.imshow(heatmap_img)
    #     plt.show()
    #     labels = label(heatmap_img)
    #     plt.imshow(labels[0])
    #     plt.show()
    #     # print(labels[1], 'cars found')
    #     draw_img, new_box = detect.draw_labeled_bboxes(np.copy(img), labels)
    #     final_img = detect.draw_boxes(img1, new_box, color=(255, 0, 255), thick=6)
    #     plt.imshow(final_img)
    #     plt.show()

    video_output = 'project_result3.mp4'
    clip1 = VideoFileClip("project_video.mp4")
    white_clip = clip1.fl_image(detect.process_vid)
    white_clip.write_videofile(video_output, audio=False)



if __name__ == '__main__':
    main()
