##Writeup Template
###You can use this file as a template for your writeup if you want to submit it as a markdown file, but feel free to use some other method and submit a pdf if you prefer.

---

**Vehicle Detection Project**

The goals / steps of this project are the following:

* Perform a Histogram of Oriented Gradients (HOG) feature extraction on a labeled training set of images and train a classifier Linear SVM classifier
* Optionally, you can also apply a color transform and append binned color features, as well as histograms of color, to your HOG feature vector. 
* Note: for those first two steps don't forget to normalize your features and randomize a selection for training and testing.
* Implement a sliding-window technique and use your trained classifier to search for vehicles in images.
* Run your pipeline on a video stream (start with the test_video.mp4 and later implement on full project_video.mp4) and create a heat map of recurring detections frame by frame to reject outliers and follow detected vehicles.
* Estimate a bounding box for vehicles detected.

[//]: # (Image References)
[image1]: ./car.jpg
[image2]: ./non_car.jpg
[image3]: ./hog.jpg
[image4]: ./not_car_hog.jpg
[image5]: ./sliding_windows.jpg
[image6]: ./heatmap.png
[image7]: ./output_boxes.jpg
[video1]: ./project_video.mp4

## [Rubric](https://review.udacity.com/#!/rubrics/513/view) Points
###Here I will consider the rubric points individually and describe how I addressed each point in my implementation.  

---
###Writeup / README

###Histogram of Oriented Gradients (HOG)

####1. Explain how (and identify where in your code) you extracted HOG features from the training images.

The code for this step is contained in the lines 36 through 53 of the file called `vehicle_detect.py`).  

I started by reading in all the `vehicle` and `non-vehicle` images.  Here is an example of one of each of the `vehicle` and `non-vehicle` classes:

![alt text][image1]
![alt text][image2]


I then explored different color spaces and different `skimage.hog()` parameters (`orientations`, `pixels_per_cell`, and `cells_per_block`).  I found that the LUV and YUV color space worked best for me. When combined with hog features, I chose LUV color space as it yielded a better result. I grabbed random images from each of the two classes and displayed them to get a feel for what the `skimage.hog()` output looks like to ensure that the edges are well detected.

Here is an example using the `LUV` color space and HOG parameters of `orientations=9`, `pixels_per_cell=(8, 8)` and `cells_per_block=(2, 2)`: (First image is of a car and second is non-car image)

![alt text][image3]
![alt text][image4]

####2. Explain how you settled on your final choice of HOG parameters.

I tried various combinations of parameters like `pixels_per_cell`, `hist_bins`(histogram bins), `hog_channels`(among 3 channels) and `orient` (number of orientations present in the final hog). For all hog channels, orientations of 9 and above, histogram bins of 32 and pixels_per_cell of 8 combinations worked the best.

####3. Describe how (and identify where in your code) you trained a classifier using your selected HOG features (and color features if you used them).

I used a standard scaler in the beginning to check the scaled value. Later, I split the data into training and test set to train the model and test the accuracy on the test set. For training, I trained a linear SVM using training data. The following output was obtained when the trained model was tested on the test data:
`Feature vector length: 6156
16.22 Seconds to train SVC...
Test Accuracy of SVC =  0.9873`

###Sliding Window Search

####1. Describe how (and identify where in your code) you implemented a sliding window search.  How did you decide what scales to search and how much to overlap windows?

Initially, the windows was run across the image width and later filter the needed windows among them based on the overlap factor. I decided to search random window positions at random scales all over the image. Here is an image with the sliding windows:

![alt text][image5]

####2. Show some examples of test images to demonstrate how your pipeline is working.  What did you do to optimize the performance of your classifier?

Ultimately I searched on two scales using `LUV` 3-channel HOG features plus spatially binned color and histograms of color in the feature vector, which provided a nice result. Here is the result of the final image where the bounding box is drawn over the detected vehicle:

![alt text][image7]
---

### Video Implementation

####1. Provide a link to your final video output.  Your pipeline should perform reasonably well on the entire project video (somewhat wobbly or unstable bounding boxes are ok as long as you are identifying the vehicles most of the time with minimal false positives.)
Here's a [link to my video result](./project_video.mp4)


####2. Describe how (and identify where in your code) you implemented some kind of filter for false positives and some method for combining overlapping bounding boxes.

I recorded the positions of positive detections in each frame of the video.  From the positive detections I created a heatmap and then thresholded that map to identify vehicle positions.  I then used `scipy.ndimage.measurements.label()` to identify individual blobs in the heatmap.  I then assumed each blob corresponded to a vehicle.  I constructed bounding boxes to cover the area of each blob detected.  

Here's an example result showing the heatmap from a series of frames of video, the result of `scipy.ndimage.measurements.label()` and the bounding boxes then overlaid on the last frame of video:

### Here are six frames and their corresponding heatmaps:

![alt text][image6]

### Here the resulting bounding boxes are drawn onto a frame in the series:
![alt text][image7]



---

###Discussion

####1. Briefly discuss any problems / issues you faced in your implementation of this project.  Where will your pipeline likely fail?  What could you do to make it more robust?

Here I'll talk about the approach I took, what techniques I used, what worked and why, where the pipeline might fail and how I might improve it if I were going to pursue this project further.  

Initially I had difficulty in choosing the color space for the hog channels. After multiple experimentation and learnings from the previous project, I chose to use `LUV` color space. RGB space was not able to identify the vehicle in color histograms. I used all of color histograms, spatial bins and hog features to identify the vehicles. Every frame of the video has its features extracted. Further, sliding windows was implemented to identify bounding boxes which overlap with the vehicles. Features were found once and sliding window was used over it. Next, a heatmap was added to ensure that there were reduced false positives to identify non-vehicle images. Finally, the single bounding box is drawn on to the original frame along with lane detection to recreate the video.

Although the pipeline is good to detect the vehicles in the project video, there could be more false positives if the terrain is different (Eg: Rainy weather, concrete road, car color in comparison with the color of the road). These varied features represents independent scenarios and in order to make the pipeline function robustly in all of them, separate test cases/parameters need to added in the pipeline to check for the scenarios and apply the parameters appropriately. 
